package com.codefirex.settings.activities;

import android.content.ContentResolver;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.UserManager;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;

import com.codefirex.settings.R;
import com.codefirex.settings.SettingsFragment;

public class InterfacePowerMenu extends SettingsFragment implements OnPreferenceChangeListener {

    private static final String TAG = "CFXSettings";

    private static final String PREF_HIDE_SCREENSHOT        =    "pref_power_menu_hide_screenshot";
    private static final String PREF_HIDE_SOUND             =    "pref_power_menu_hide_sound";
    private static final String PREF_HIDE_AIRPLANE_MODE     =    "pref_power_menu_hide_airplane_mode";
    private static final String PREF_HIDE_REBOOT_MENU       =    "pref_power_menu_hide_reboot_menu";
    private static final String PREF_SHOW_SCREENSHOT_DELAY  =    "pref_power_menu_screenshot_delay";
    private static final String KEY_EXPANDED_DESKTOP = "power_menu_expanded_desktop";
    private static final String KEY_USER = "power_menu_user";
    private static final String PREF_NAVBAR_HIDE = "show_navbar_hide";

    private static final int HIDE_REBOOT = 1;
    private static final int HIDE_SCREENSHOT = 2;
    private static final int HIDE_SOUND = 4;
    private static final int HIDE_AIRPLANE = 8;

    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;

    private CheckBoxPreference mExpandedDesktopPref;
    private CheckBoxPreference mHideScreenshot;
    private CheckBoxPreference mHideSound;
    private CheckBoxPreference mHideAirplaneMode;
    private CheckBoxPreference mHideRebootMenu;
    private CheckBoxPreference mUserPref;
    CheckBoxPreference mShowNavBarHide;
    //private NumberPickerPreference mScreenshotDelay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.interface_power_menu);

        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        int hiddenOptions = Settings.System.getInt(getContext().getContentResolver(),
                Settings.System.HIDDEN_POWER_MENU_OPTIONS, 0);

        /* Hide Screenshot Menu */
        mHideScreenshot = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_HIDE_SCREENSHOT);
        mHideScreenshot.setChecked((hiddenOptions & HIDE_SCREENSHOT) != 0);

        /* Hide Sound Menu */
        mHideSound = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_HIDE_SOUND);
        mHideSound.setChecked((hiddenOptions & HIDE_SOUND) != 0);

        /* Hide Airplane Mode Menu */
        mHideAirplaneMode = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_HIDE_AIRPLANE_MODE);
        mHideAirplaneMode.setChecked((hiddenOptions & HIDE_AIRPLANE) != 0);

        /* Hide Reboot Menu */
        mHideRebootMenu = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_HIDE_REBOOT_MENU);
        mHideRebootMenu.setChecked((hiddenOptions & HIDE_REBOOT) != 0);

        /* Show Expanded Desktop */
        mExpandedDesktopPref = (CheckBoxPreference) findPreference(KEY_EXPANDED_DESKTOP);
        mExpandedDesktopPref.setChecked((Settings.System.getInt(getContentResolver(),
                Settings.System.POWER_MENU_EXPANDED_DESKTOP_ENABLED, 0) == 1));

        /* Show User Picker */
        mUserPref = (CheckBoxPreference) mPrefSet.findPreference(KEY_USER);
        mUserPref.setChecked((Settings.System.getInt(getContentResolver(),
                Settings.System.POWER_MENU_USER_ENABLED, 0) == 1));

        /* Show NavBar Controls */
        mShowNavBarHide = (CheckBoxPreference) mPrefSet.findPreference(PREF_NAVBAR_HIDE);
        mShowNavBarHide.setChecked(Settings.System.getBoolean(getActivity()
                .getContentResolver(), Settings.System.POWER_DIALOG_SHOW_NAVBAR_HIDE,
                false));

        //mScreenshotDelay = (NumberPickerPreference) mPrefSet.findPreference(
        //        PREF_SHOW_SCREENSHOT_DELAY);
        //mScreenshotDelay.setDependency(PREF_SHOW_SCREENSHOT);
        //mScreenshotDelay.setOnPreferenceChangeListener(this);
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        boolean value;

        if (preference == mHideScreenshot ||
                preference == mHideSound ||
                preference == mHideAirplaneMode ||
                preference == mHideRebootMenu) {
            int options = 0;
            if (mHideScreenshot.isChecked()) options |= HIDE_SCREENSHOT;
            if (mHideAirplaneMode.isChecked()) options |= HIDE_AIRPLANE;
            if (mHideRebootMenu.isChecked()) options |= HIDE_REBOOT;
            if (mHideSound.isChecked()) options |= HIDE_SOUND;
            Settings.System.putInt(mCr, Settings.System.HIDDEN_POWER_MENU_OPTIONS,
                    options);
        } else if (preference == mUserPref) {
            value = mUserPref.isChecked();
            Settings.System.putInt(getContentResolver(),
                    Settings.System.POWER_MENU_USER_ENABLED,
                    value ? 1 : 0);
        } else if (preference == mExpandedDesktopPref) {
            value = mExpandedDesktopPref.isChecked();
            Settings.System.putInt(getContentResolver(),
                    Settings.System.POWER_MENU_EXPANDED_DESKTOP_ENABLED,
                    value ? 1 : 0);
        } else if (preference == mShowNavBarHide) {
            Settings.System.putBoolean(getActivity().getContentResolver(),
                    Settings.System.POWER_DIALOG_SHOW_NAVBAR_HIDE,
                    ((CheckBoxPreference)preference).isChecked());
        } else {
            return super.onPreferenceTreeClick(preferenceScreen, preference);
        }
        return true;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        //if (preference == mScreenshotDelay) {
        //    int value = Integer.parseInt(newValue.toString());
        //    Settings.System.putInt(mCr, Settings.System.POWER_MENU_SCREENSHOT_DELAY,
        //            value);
        //}
        return false;
    }
}
