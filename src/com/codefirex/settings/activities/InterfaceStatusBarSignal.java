
package com.codefirex.settings.activities;

import com.codefirex.settings.colorpicker.ColorPickerPreference;
import android.content.ContentResolver;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceChangeListener;
import android.provider.Settings;
import android.util.Log;

import com.codefirex.settings.R;
import com.codefirex.settings.CFXPreferenceFragment;

public class InterfaceStatusBarSignal extends CFXPreferenceFragment implements
        OnPreferenceChangeListener {

    private static final String CAT_SIGNAL_COLOR="signal_color_cat";
    private static final String PREF_SIGNAL_STYLE = "signal_style";
    private static final String PREF_SIGNAL_COLOR = "signal_color";
    private static final String PREF_WIFI_SIGNAL_STYLE = "wifi_signal_style";
    private static final String PREF_WIFI_SIGNAL_COLOR = "wifi_signal_color";
    private static final String PREF_HIDE_SIGNAL = "hide_signal";
    private static final String PREF_SIXBAR_SIGNAL = "sixbar_signal";
    private static final String PREF_SIXBAR_WIFI_SIGNAL = "sixbar_wifi_signal";

    private boolean mNoSignalText;
    private boolean mNoWifiSignalText;
    private CheckBoxPreference mSixBarSignal;
    private CheckBoxPreference mSixBarWifiSignal;
    ListPreference mDbmStyletyle;
    ListPreference mWifiStyle;
    ColorPickerPreference mColorPicker;
    ColorPickerPreference mWifiColorPicker;
    CheckBoxPreference mHideSignal;

    private ContentResolver mCr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.interface_statusbarsignal);

        PreferenceScreen prefSet = getPreferenceScreen();
        mCr = getContentResolver();

        /** Signal style */
        mDbmStyletyle = (ListPreference) prefSet.findPreference(PREF_SIGNAL_STYLE);
        mDbmStyletyle.setOnPreferenceChangeListener(this);
        mDbmStyletyle.setValue(Integer.toString(Settings.System.getInt(mCr,
                        Settings.System.STATUSBAR_SIGNAL_TEXT, 0)));

        /** WiFi style */
        mWifiStyle = (ListPreference) prefSet.findPreference(PREF_WIFI_SIGNAL_STYLE);
        mWifiStyle.setOnPreferenceChangeListener(this);
        mWifiStyle.setValue(Integer.toString(Settings.System.getInt(mCr,
                        Settings.System.STATUSBAR_WIFI_SIGNAL_TEXT, 0)));

        /** Hide signal toggle */
        mHideSignal = (CheckBoxPreference) prefSet.findPreference(PREF_HIDE_SIGNAL);
        mHideSignal.setChecked(Settings.System.getInt(mCr,
                    Settings.System.STATUSBAR_HIDE_SIGNAL_BARS, 0) != 0);

        /** Sixbar signal toggle */
        mSixBarSignal = (CheckBoxPreference) prefSet.findPreference(PREF_SIXBAR_SIGNAL);
        mSixBarSignal.setChecked(Settings.System.getInt(mCr,
                    Settings.System.STATUSBAR_SIXBAR_SIGNAL, 0) == 1);

        /** Sixbar wifi signal toggle */
        mSixBarWifiSignal = (CheckBoxPreference) prefSet.findPreference(PREF_SIXBAR_WIFI_SIGNAL);
        mSixBarWifiSignal.setChecked(Settings.System.getInt(mCr,
                    Settings.System.STATUSBAR_SIXBAR_WIFI_SIGNAL, 0) == 1);

        /** Signal text color */
        mColorPicker = (ColorPickerPreference) prefSet.findPreference(PREF_SIGNAL_COLOR);
        mColorPicker.setOnPreferenceChangeListener(this);

        /** WiFi text color */
        mWifiColorPicker = (ColorPickerPreference) prefSet.findPreference(PREF_WIFI_SIGNAL_COLOR);
        mWifiColorPicker.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        if (preference == mHideSignal) {
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_HIDE_SIGNAL_BARS,
                    ((CheckBoxPreference) preference).isChecked() ? 1 : 0);
            return true;
        } else if (preference == mSixBarSignal) {
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_SIXBAR_SIGNAL,
                    ((CheckBoxPreference) preference).isChecked() ? 1 : 0);
            return true;
        } else if (preference == mSixBarWifiSignal) {
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_SIXBAR_WIFI_SIGNAL,
                    ((CheckBoxPreference) preference).isChecked() ? 1 : 0);
            return true;
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mDbmStyletyle) {

            int val = Integer.parseInt((String) newValue);
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_SIGNAL_TEXT, val);
            return true;
        } else if (preference == mColorPicker) {
            String hex = ColorPickerPreference.convertToARGB(Integer.valueOf(String
                    .valueOf(newValue)));
            preference.setSummary(hex);

            int intHex = ColorPickerPreference.convertToColorInt(hex);
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_SIGNAL_TEXT_COLOR, intHex);
            return true;
        } else if (preference == mWifiStyle) {
            int val = Integer.parseInt((String) newValue);
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_WIFI_SIGNAL_TEXT, val);
            return true;
        } else if (preference == mWifiColorPicker) {
            String hex = ColorPickerPreference.convertToARGB(Integer.valueOf(String
                    .valueOf(newValue)));
            preference.setSummary(hex);

            int intHex = ColorPickerPreference.convertToColorInt(hex);
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_WIFI_SIGNAL_TEXT_COLOR, intHex);
            return true;
        }
        return false;
    }
}
