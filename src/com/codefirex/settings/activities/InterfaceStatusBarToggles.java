
package com.codefirex.settings.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.CursorLoader;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.provider.ContactsContract;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.content.ContentResolver;
import com.codefirex.settings.CFXPreferenceFragment;
import com.codefirex.settings.R;
import com.codefirex.settings.widgets.TouchInterceptor;
import com.codefirex.settings.widgets.SeekBarPreference;
import com.scheffsblend.smw.Preferences.ImageListPreference;
import com.codefirex.settings.colorpicker.ColorPickerPreference;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class InterfaceStatusBarToggles extends CFXPreferenceFragment implements
        OnPreferenceChangeListener {

    private static final String TAG = "TogglesLayout";

    private static final String PREF_ENABLE_TOGGLES = "enabled_toggles";
    private static final String PREF_TOGGLES_PER_ROW = "toggles_per_row";
    private static final String PREF_ENABLE_PW = "pref_enable_powerwidget_toggle";
    private static final String PREF_PW_TOGGLES = "pw_toggles";
    private static final String PREF_ENABLED_PW_TOGGLES = "enabled_pw_toggles";
    private static final String PREF_TOGGLE_FAV_CONTACT = "toggle_fav_contact";
    private static final String PREF_PW_HIDEBAR = "pref_powerwidget_hidebar";
    private static final String PREF_PW_HAPTIC = "pref_powerwidget_haptic";
    private static final String PREF_PW_NOSCROLL = "pref_powerwidget_disablescroll";


    private final int PICK_CONTACT = 1;

    Preference mEnabledToggles;
    Preference mLayout;
    ListPreference mTogglesPerRow;
    Preference mFavContact;
    CheckBoxPreference mEnablePW;
    CheckBoxPreference mPWHideBar;
    CheckBoxPreference mPWHaptic;
    CheckBoxPreference mPWNoScroll;
    Preference mPWEnabledToggles;
    Preference mPWLayout;
    ContentResolver mCr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	mCr = getContentResolver();
        setTitle(R.string.pref_interface_status_bar_toggles_title);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.interface_statusbar_toggles);

        mEnabledToggles = findPreference(PREF_ENABLE_TOGGLES);
	mPWEnabledToggles = findPreference(PREF_ENABLED_PW_TOGGLES);

	mPWHideBar = (CheckBoxPreference)findPreference(PREF_PW_HIDEBAR);
	mPWHideBar.setChecked(Settings.Secure.getInt(getContentResolver(),
                Settings.Secure.EXPANDED_HIDE_SCROLLBAR, 0) == 1);

	mPWHaptic = (CheckBoxPreference)findPreference(PREF_PW_HAPTIC);
	mPWHaptic.setChecked(Settings.Secure.getInt(getContentResolver(),
                Settings.Secure.EXPANDED_HAPTIC_FEEDBACK, 1) == 1);

	mPWNoScroll = (CheckBoxPreference)findPreference(PREF_PW_NOSCROLL);
	mPWNoScroll.setChecked(Settings.Secure.getInt(getContentResolver(),
                Settings.Secure.EXPANDED_NOSCROLL, 0) == 1);

        mTogglesPerRow = (ListPreference) findPreference(PREF_TOGGLES_PER_ROW);
        mTogglesPerRow.setValue(Settings.System.getInt(getActivity().getContentResolver(),
                Settings.System.QUICK_TOGGLES_PER_ROW, 3) + "");
        mTogglesPerRow.setOnPreferenceChangeListener(this);

        mLayout = findPreference("toggles");
	mPWLayout = findPreference(PREF_PW_TOGGLES);

        mFavContact = findPreference(PREF_TOGGLE_FAV_CONTACT);

        final String[] entries = getResources().getStringArray(R.array.available_toggles_entries);

        List<String> allToggles = Arrays.asList(entries);

	mEnablePW = (CheckBoxPreference)findPreference(PREF_ENABLE_PW);
	mEnablePW.setChecked(Settings.Secure.getInt(getContentResolver(),
                Settings.Secure.EXPANDED_VIEW_WIDGET, 0) == 1);
    }
  
    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mTogglesPerRow) {
            int val = Integer.parseInt((String) newValue);
            Settings.System.putInt(getActivity().getContentResolver(),
                    Settings.System.QUICK_TOGGLES_PER_ROW, val);
            return true;
        } 
        return false;
    }
void createAlertDialogForNewToggles()
{
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            ArrayList<String> enabledToggles = getTogglesStringArray(getActivity());

            final String[] finalArray = getResources().getStringArray(
                    R.array.available_toggles_entries);
            final String[] values = getResources().getStringArray(R.array.available_toggles_values);

            boolean checkedToggles[] = new boolean[finalArray.length];

            for (int i = 0; i < checkedToggles.length; i++) {
                if (enabledToggles.contains(finalArray[i])) {
                    checkedToggles[i] = true;
                }
            }

            builder.setTitle(R.string.toggles_display_dialog);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.toggles_display_close,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.setMultiChoiceItems(values, checkedToggles, new OnMultiChoiceClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    String toggleKey = (finalArray[which]);

                    if (isChecked)
                        addToggle(getActivity(), toggleKey);
                    else
                        removeToggle(getActivity(), toggleKey);
                }
            });

            AlertDialog d = builder.create();

            d.show();
}
void createAlertDialogForOldToggles()
{
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            ArrayList<String> enabledToggles = getPWTogglesStringArray(getActivity());

            final String[] finalArray = getResources().getStringArray(
                    R.array.available_pw_toggles_entries);
            final String[] values = getResources().getStringArray(R.array.available_pw_toggles_values);

            boolean checkedToggles[] = new boolean[finalArray.length];

            for (int i = 0; i < checkedToggles.length; i++) {
                if (enabledToggles.contains(finalArray[i])) {
                    checkedToggles[i] = true;
                }
            }

            builder.setTitle(R.string.toggles_display_dialog);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.toggles_display_close,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.setMultiChoiceItems(values, checkedToggles, new OnMultiChoiceClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    String toggleKey = (finalArray[which]);

                    if (isChecked)
                        addPWToggle(getActivity(), toggleKey);
                    else
                        removePWToggle(getActivity(), toggleKey);
                }
            });

            AlertDialog d = builder.create();

            d.show();
}
    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mEnabledToggles) {
	    createAlertDialogForNewToggles();
            return true;
        } else if (preference == mPWEnabledToggles) {
	    createAlertDialogForOldToggles();
            return true;
        } else if (preference == mLayout) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            TogglesLayout fragment = new TogglesLayout();
            ft.addToBackStack("toggles_layout");
            ft.replace(this.getId(), fragment);
            ft.commit();
        } else if (preference == mPWLayout) {
            FragmentTransaction ft2 = getFragmentManager().beginTransaction();
            PWTogglesLayout f = new PWTogglesLayout();
            ft2.addToBackStack("pw_toggles_layout");
            ft2.replace(this.getId(), f);
            ft2.commit();
        } else if (preference == mFavContact) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        } else if (preference == mEnablePW) {
	    boolean value = mEnablePW.isChecked();
            Settings.Secure.putInt(mCr, Settings.Secure.EXPANDED_VIEW_WIDGET,
                    value ? 1 : 0);
	    return true;
	} else if (preference == mPWHideBar) {
	    boolean value = mPWHideBar.isChecked();
            Settings.Secure.putInt(mCr, Settings.Secure.EXPANDED_HIDE_SCROLLBAR,
                    value ? 1 : 0);
	    return true;
	} else if (preference == mPWHaptic) {
	    boolean value = mPWHaptic.isChecked();
            Settings.Secure.putInt(mCr, Settings.Secure.EXPANDED_HAPTIC_FEEDBACK,
                    value ? 1 : 0);
	    return true;
	} else if (preference == mPWNoScroll) {
	    boolean value = mPWNoScroll.isChecked();
            Settings.Secure.putInt(mCr, Settings.Secure.EXPANDED_NOSCROLL,
                    value ? 1 : 0);
	    return true;
	}
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_CONTACT) {
                Uri contactData = data.getData();
                String[] projection = new String[] {ContactsContract.Contacts.LOOKUP_KEY};
                String selection = ContactsContract.Contacts.DISPLAY_NAME + " IS NOT NULL";
                CursorLoader cursorLoader =  new CursorLoader(getActivity().getBaseContext(), contactData, projection, selection, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                if (cursor != null) {
                    try {
                        if (cursor.moveToFirst()) {
                            String lookup_key = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                            Settings.System.putString(getActivity().getContentResolver(),
                            Settings.System.QUICK_TOGGLE_FAV_CONTACT, lookup_key);
                        }
                    } finally {
                        cursor.close();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void addToggle(Context context, String key) {
        ArrayList<String> enabledToggles = getTogglesStringArray(context);
        enabledToggles.add(key);
        setTogglesFromStringArray(context, enabledToggles);
    }

    public void removeToggle(Context context, String key) {
        ArrayList<String> enabledToggles = getTogglesStringArray(context);
        enabledToggles.remove(key);
        setTogglesFromStringArray(context, enabledToggles);
    }
    public void addPWToggle(Context context, String key) {
        ArrayList<String> enabledToggles = getPWTogglesStringArray(context);
        enabledToggles.add(key);
        setPWTogglesFromStringArray(context, enabledToggles);
    }

    public void removePWToggle(Context context, String key) {
        ArrayList<String> enabledToggles = getPWTogglesStringArray(context);
        enabledToggles.remove(key);
        setPWTogglesFromStringArray(context, enabledToggles);
    }
    public class TogglesLayout extends ListFragment {

        private ListView mButtonList;
        private ButtonAdapter mButtonAdapter;
        private Context mContext;

        /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);

            mContext = getActivity().getBaseContext();

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.order_power_widget_buttons_activity, container,
                    false);

            return v;
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            mButtonList = this.getListView();
            ((TouchInterceptor) mButtonList).setDropListener(mDropListener);
            mButtonAdapter = new ButtonAdapter(mContext);
            setListAdapter(mButtonAdapter);
        };

        @Override
        public void onDestroy() {
            ((TouchInterceptor) mButtonList).setDropListener(null);
            setListAdapter(null);
            super.onDestroy();
        }

        @Override
        public void onResume() {
            super.onResume();
            // reload our buttons and invalidate the views for redraw
            mButtonAdapter.reloadButtons();
            mButtonList.invalidateViews();
        }

        private TouchInterceptor.DropListener mDropListener = new TouchInterceptor.DropListener() {
            public void drop(int from, int to) {
                // get the current button list
                ArrayList<String> toggles = getTogglesStringArray(mContext);

                // move the button
                if (from < toggles.size()) {
                    String toggle = toggles.remove(from);

                    if (to <= toggles.size()) {
                        toggles.add(to, toggle);

                        // save our buttons
                        setTogglesFromStringArray(mContext, toggles);

                        // tell our adapter/listview to reload
                        mButtonAdapter.reloadButtons();
                        mButtonList.invalidateViews();
                    }
                }
            }
        };

        private class ButtonAdapter extends BaseAdapter {
            private Context mContext;
            private Resources mSystemUIResources = null;
            private LayoutInflater mInflater;
            private ArrayList<Toggle> mToggles;

            public ButtonAdapter(Context c) {
                mContext = c;
                mInflater = LayoutInflater.from(mContext);

                PackageManager pm = mContext.getPackageManager();
                if (pm != null) {
                    try {
                        mSystemUIResources = pm.getResourcesForApplication("com.android.systemui");
                    } catch (Exception e) {
                        mSystemUIResources = null;
                        Log.e(TAG, "Could not load SystemUI resources", e);
                    }
                }

                reloadButtons();
            }

            public void reloadButtons() {
                ArrayList<String> toggles = getTogglesStringArray(mContext);
                final String[] finalArray = getResources().getStringArray(
                        R.array.available_toggles_entries);

                mToggles = new ArrayList<Toggle>();
                for (String toggle : toggles) {
                    for (int i=0;i < finalArray.length;i++) {
                        if(finalArray[i].equals(toggle)) {
                            mToggles.add(new Toggle(toggle, i));
                        }
                    }
                }
            }

            public int getCount() {
                return mToggles.size();
            }

            public Object getItem(int position) {
                return mToggles.get(position);
            }

            public long getItemId(int position) {
                return position;
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                final View v;
                if (convertView == null) {
                    v = mInflater.inflate(R.layout.order_power_widget_button_list_item, null);
                } else {
                    v = convertView;
                }

                Toggle toggle = mToggles.get(position);
                final TextView name = (TextView) v.findViewById(R.id.name);
                final String[] values = getResources().getStringArray(
                        R.array.available_toggles_values);
                name.setText(values[toggle.getTitleResId()]);
                return v;
            }
        }

    }
    public class PWTogglesLayout extends ListFragment {

        private ListView mButtonList;
        private ButtonAdapter mButtonAdapter;
        private Context mContext;

        /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);

            mContext = getActivity().getBaseContext();

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.order_power_widget_buttons_activity, container,
                    false);

            return v;
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            mButtonList = this.getListView();
            ((TouchInterceptor) mButtonList).setDropListener(mDropListener);
            mButtonAdapter = new ButtonAdapter(mContext);
            setListAdapter(mButtonAdapter);
        };

        @Override
        public void onDestroy() {
            ((TouchInterceptor) mButtonList).setDropListener(null);
            setListAdapter(null);
            super.onDestroy();
        }

        @Override
        public void onResume() {
            super.onResume();
            // reload our buttons and invalidate the views for redraw
            mButtonAdapter.reloadButtons();
            mButtonList.invalidateViews();
        }

        private TouchInterceptor.DropListener mDropListener = new TouchInterceptor.DropListener() {
            public void drop(int from, int to) {
                // get the current button list
                ArrayList<String> toggles = getPWTogglesStringArray(mContext);

                // move the button
                if (from < toggles.size()) {
                    String toggle = toggles.remove(from);

                    if (to <= toggles.size()) {
                        toggles.add(to, toggle);

                        // save our buttons
                        setPWTogglesFromStringArray(mContext, toggles);

                        // tell our adapter/listview to reload
                        mButtonAdapter.reloadButtons();
                        mButtonList.invalidateViews();
                    }
                }
            }
        };

        private class ButtonAdapter extends BaseAdapter {
            private Context mContext;
            private Resources mSystemUIResources = null;
            private LayoutInflater mInflater;
            private ArrayList<Toggle> mToggles;

            public ButtonAdapter(Context c) {
                mContext = c;
                mInflater = LayoutInflater.from(mContext);

                PackageManager pm = mContext.getPackageManager();
                if (pm != null) {
                    try {
                        mSystemUIResources = pm.getResourcesForApplication("com.android.systemui");
                    } catch (Exception e) {
                        mSystemUIResources = null;
                        Log.e(TAG, "Could not load SystemUI resources", e);
                    }
                }

                reloadButtons();
            }

            public void reloadButtons() {
                ArrayList<String> toggles = getPWTogglesStringArray(mContext);
                final String[] finalArray = getResources().getStringArray(
                        R.array.available_pw_toggles_entries);

                mToggles = new ArrayList<Toggle>();
                for (String toggle : toggles) {
                    for (int i=0;i < finalArray.length;i++) {
                        if(finalArray[i].equals(toggle)) {
                            mToggles.add(new Toggle(toggle, i));
                        }
                    }
                }
            }

            public int getCount() {
                return mToggles.size();
            }

            public Object getItem(int position) {
                return mToggles.get(position);
            }

            public long getItemId(int position) {
                return position;
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                final View v;
                if (convertView == null) {
                    v = mInflater.inflate(R.layout.order_power_widget_button_list_item, null);
                } else {
                    v = convertView;
                }

                Toggle toggle = mToggles.get(position);
                final TextView name = (TextView) v.findViewById(R.id.name);
                final String[] values = getResources().getStringArray(
                        R.array.available_pw_toggles_values);
                name.setText(values[toggle.getTitleResId()]);
                return v;
            }
        }

    }

    public static class Toggle {
        private String mId;
        private int mTitleResId;

        public Toggle(String id, int titleResId) {
            mId = id;
            mTitleResId = titleResId;
        }

        public String getId() {
            return mId;
        }

        public int getTitleResId() {
            return mTitleResId;
        }

    }

    public void setTogglesFromStringArray(Context c, ArrayList<String> newGoodies) {
        String newToggles = "";

        for (String s : newGoodies)
            newToggles += s + "|";

        // remote last |
        try {
            newToggles = newToggles.substring(0, newToggles.length() - 1);
        } catch (StringIndexOutOfBoundsException e) {
        }

        Settings.System.putString(c.getContentResolver(), Settings.System.QUICK_TOGGLES,
                newToggles);
    }
    public void setPWTogglesFromStringArray(Context c, ArrayList<String> newGoodies) {
        String newToggles = "";

        for (String s : newGoodies)
            newToggles += s + "|";

        // remote last |
        try {
            newToggles = newToggles.substring(0, newToggles.length() - 1);
        } catch (StringIndexOutOfBoundsException e) {
        }

        Settings.Secure.putString(c.getContentResolver(), Settings.Secure.WIDGET_BUTTONS,
                newToggles);
    }
    public ArrayList<String> getTogglesStringArray(Context c) {
        String clusterfuck = Settings.System.getString(c.getContentResolver(),
                Settings.System.QUICK_TOGGLES);

        if (clusterfuck == null) {
            Log.e(TAG, "clusterfuck was null");
            // return null;
            clusterfuck = getResources().getString(R.string.toggle_default_entries);
        }

        String[] togglesStringArray = clusterfuck.split("\\|");
        ArrayList<String> iloveyou = new ArrayList<String>();
        for (String s : togglesStringArray) {
            if(s != null && s != "") {
                Log.e(TAG, "adding: " + s);
                iloveyou.add(s);
            }
        }
        return iloveyou;
    }
    public ArrayList<String> getPWTogglesStringArray(Context c) {
        String clusterfuck = Settings.Secure.getString(c.getContentResolver(),
                Settings.Secure.WIDGET_BUTTONS);

        if (clusterfuck == null) {
            Log.e(TAG, "clusterfuck was null");
            // return null;
            clusterfuck = "toggleWifi|toggleBluetooth|toggleGPS|toggleSound";
        }

        String[] togglesStringArray = clusterfuck.split("\\|");
        ArrayList<String> iloveyou = new ArrayList<String>();
        for (String s : togglesStringArray) {
            if(s != null && s != "") {
                Log.e(TAG, "adding: " + s);
                iloveyou.add(s);
            }
        }
        return iloveyou;
    }
}
